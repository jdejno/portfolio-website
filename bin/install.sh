#!/bin/bash

NODEDIR="./node_modules"
BOWERDIR="./bower_components"

if [ ! -d "$NODEDIR" ]; then
  echo "Running npm install..."
  npm install

fi

if [ ! -d "$BOWERDIR" ]; then
  echo "Running bower install..."
  bower install

fi
