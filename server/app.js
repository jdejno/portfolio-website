const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const winston = require('winston'); //TODO: Configure better logging using Winston and Morgan (https://stackoverflow.com/questions/27906551/node-js-logging-use-morgan-and-winston)
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');


//main app
let app = express();

//#region view engines
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//#endregion

//#region middleware

app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//#endregion

//#region static serving

//if we are running in developer mode, server static files from build rather than dist
if(app.get('env') === 'development' ){

  app.use(express.static(path.resolve(__dirname, '../build')));

} else {

  app.use(express.static(path.resolve(__dirname, '../dist')));

}

//#endregion

//#region routes

//import custom routes
const projects = require('./routes/projects/projects');
const blog = require('./routes/blog/posts');
const gitCode = require('./routes/git-code');
app.use('/api/projects', projects);
app.use('/api/blog', blog);
app.use('/api/code', gitCode);

//#endregion

//#region error handling

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//#endregion

module.exports = app;
