/**
 * Created by jeremydejno on 10/7/17.
 */
//#region imports
let express = require('express');
let router = express.Router();
const request = require('request');
const logger = require('../logging/debug-logger');

//#endregion

/**
 * Get/construct URL to remote repo based on the git platform requested
 * @param git {number} -  number corresponding to git platform.
 * @param user {string} - owner of repo
 * @param repo {string} - remote repo name
 * @param path {string} - absolute path to file in remote repo
 * @returns {string} - URL to api of git platform.
 */
let getGitURL = (git, user, repo, path) => {
  "use strict";
  switch (git)
  {

    case 0:
      return encodeURI(`https://api.bitbucket.org/2.0/repositories/${user}/${repo}/src/master/${path}`);
    default: //bitbucket default
      return encodeURI(`https://api.bitbucket.org/2.0/repositories/${user}/${repo}/src/master/${path}`);
  }

};

/**
 * Get raw code files from Bitbucket and Github using their API.
 */
router.get('/raw/:git/:repo', (req, res, next) => {

  "use strict";
  let git = +req.params.git,
    repo = req.params.repo,
    path = req.query.path,
    user = req.query.user;

  let url = getGitURL(git, user, repo, path);

  //get raw code from remote repos
  request(url, (error, response, body) => {
      if(response.statusCode === 200) {

        res.send(body);

      } else {

        next(error);

      }
    });

});


module.exports = router;
