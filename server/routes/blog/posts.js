/**
 * Created by jeremydejno on 9/10/17.
 */
const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const winston = require('winston');

let logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: 'debug' }),
  ]
});


/**
 * GET all blog posts or single post is query 'post' parameter is supplied
 *
 */
router.get('/posts', function (req, res, next) {
  //get json file with post metadata
  let data = require('../../public/data/blog/posts.json');

  //if we have a query param
  if(req.query.hasOwnProperty("post")){

    let postID = +req.query['post'];

    let post = data.find(function(d, i) {return d.id === postID});

    if(post === undefined) {

      post = {};
      post.id = 0;
      post.date = new Date(Date.now());
      post.snippet = "No Post For Id " + postID;
      post.title = "Whoops! " + post.snippet;
      post['content-file'] = 'no-post-default.html';

    } else {
      post.date = new Date(post.date);
    }

    let htmlFile = post['content-file'];

    if(htmlFile !== undefined){

      let htmlFilePath = path.resolve(__dirname,'../../public/data/blog/posts/', htmlFile);

      fs.readFile(htmlFilePath, "utf8", function(err, data) {
        "use strict";
        if(err) logger.debug(err);
        post.content = data;
        res.json(post);
      });

    } else {

      res.json(post);

    }

  } else {    //send all the posts metadata if we don't have a query parameters
    //parse dates
    data = data.map((d, i) => {
      d.date = new Date(d.date);
      return d;
    });
    res.json(data);

  }

});

module.exports = router;
