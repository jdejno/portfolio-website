let express = require('express');
let router = express.Router();
let projects = require('../../public/data/projects/projects.json');
const csv = require('csvtojson');
const path = require('path');

/* GET projects metadata */
router.get('/', function(req, res, next) {
  res.json(projects);
});

/**
 * GET single project data.
 * TODO: Transfer data over to a better format for all projects.
 */
router.get('/data/:project', (req, res, next) => {
  let data = [];

  switch (req.params.project.toUpperCase()) {

    case 'USA-MAP':
      data = require('../../public/data/projects/usa.json');
      res.send(data);
      break;

    case 'LIFELINE':
      let file = path.resolve(__dirname, '../../public/data/projects/lifeline-data.csv');
      console.log(file);
      csv({noHeader: false})
        .fromFile(file)
        .on('json', function (obj) {
          data.push(obj);
        })
        .on('end', function () {
          res.json(data);
        });
      break;

    default:
      break;
  }

});


module.exports = router;
