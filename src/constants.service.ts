import { Injectable } from '@angular/core';

@Injectable()
export class CONSTANTS  {

  public LOGO_URL: string = '/assets/images/jd_logo_white.svg';
  public PRIMARY_COLOR: string = '#DF691A';

}

