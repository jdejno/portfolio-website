import { Component} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {environment} from "../environments/environment";

//gtag from Google Analytics
declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
blog
  constructor(private router: Router) {
    //if running in prod mode, do Google Analytics
    if(environment.production){
      router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          //Google Analytics for page changes
          gtag('config', 'UA-106965223-1', {
            'page_path': router.url
          });

        }
      });
    }


  }


}
