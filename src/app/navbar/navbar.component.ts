import {Component, OnInit, HostListener, ElementRef, ViewChild, AfterViewInit, Input} from '@angular/core';
import {ProjectService} from "../projects/providers/project.service";
import $ from 'jquery';
import { Router, NavigationEnd} from "@angular/router";
import {NavbarSettingsService, cssAttributes} from "./navbar-settings.service";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],

})
export class NavbarComponent implements OnInit {

  @ViewChild('navbar') navbar: ElementRef;
  @ViewChild('collapseableNavbar') collapseableNavbar: ElementRef;

  public projects: any[];
  public moreProjects: boolean = false;
  public cssAttributes: cssAttributes;

  constructor(private _el: ElementRef,
              private _projectService: ProjectService,
              private router: Router,
              private _navbarSettings: NavbarSettingsService) {

    this._navbarSettings.subscribe(this);

    //workaround to enable scrolling into view when route.fragment is not in current component.
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = <HTMLElement>document.querySelector("#" + tree.fragment);
          if (element) {

              element.scrollIntoView();

          }
        }

        //Handle closing of collapsed menu on small screen after navigating to a navbar link
        if (this.collapseableNavbar.nativeElement.attributes.getNamedItem('aria-expanded').value === 'true'){
          this.collapseableNavbar.nativeElement.click();
        }

      }

    });

  }


  ngOnInit() {
    //get projects to display in project dropdown menu.
    this._projectService.getAllProjects().then(res => {
      this.projects = res;
      if (this.projects.length > 5) {
        this.projects = this.projects.slice(0, 4);
        this.moreProjects = true
      }
    });

  }

  /**
   * On document scroll, fade out navbar.
   * @param ev
   */
  @HostListener('document:scroll', ['$event'])
  onScroll(ev) {
    let scrollTarget = 500;
    let targetScroll = ev.target.scrollingElement.scrollTop;
    let navbar = this.navbar.nativeElement;

    if(targetScroll >= scrollTarget) {
      $(navbar).css('display', 'none')
    } else {
      $(navbar).css({
        'display': 'block',
          'opacity':
        ((scrollTarget - (targetScroll > scrollTarget ?  scrollTarget : targetScroll ))
          / scrollTarget)
        });
    }
  }



}
