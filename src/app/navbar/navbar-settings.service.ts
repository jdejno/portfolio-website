import {Injectable} from '@angular/core';
import {Observable, BehaviorSubject} from "rxjs";
import {NavbarComponent} from "./navbar.component";

export interface cssAttributes {
  fontColor: string
}

@Injectable()
export class NavbarSettingsService {

  private _observable: BehaviorSubject<cssAttributes>;
  private _cssAttributes: cssAttributes;

  constructor() {
    this._cssAttributes = {
      fontColor: 'lightgrey'
    };
    this._observable = new BehaviorSubject<cssAttributes>(this._cssAttributes);
  }

  //#region public methods

  public subscribe(subscriber: NavbarComponent) {
    this._observable.subscribe({
      next: x => {
        subscriber.cssAttributes = x
      }
    })
  }

  public set fontColor(color: string) {
    this._cssAttributes.fontColor = color;
    this._observable.next(this._cssAttributes)
  }


  //#endregion

}
