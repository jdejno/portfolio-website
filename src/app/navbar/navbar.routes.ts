/**
 * Created by jeremydejno on 5/7/17.
 */
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "../home/home.component";
import {ProjectComponent} from "../projects/project.component";
import {BlogComponent} from "../blog/blog.component";
import {AboutComponent} from "../home/about/about.component";
import {ProjectsModule} from "../projects/projects.module";

const navbarRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'projects', component: ProjectComponent},
];

export const navbarRouting = RouterModule.forRoot(navbarRoutes);
