import {Component, OnInit, ViewChild} from '@angular/core';
import {trigger, state, transition, style, animate} from "@angular/animations";
import {Router} from "@angular/router";
import {NavbarSettingsService} from "../../navbar/navbar-settings.service";
import {LogoAnimationService, Logo, ELogoAnimations} from "../../projects/logo-animation/logo-animation.module";

/**
 * Header section of Home page.
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [LogoAnimationService],
  animations: [
    trigger('fadeIn', [
      state('visible', style({opacity: '*'})),
      state('invisible', style({opacity: 0})),
      transition('void => *', [
        style({opacity: 0}),
        animate("1500ms 500ms", style({opacity: '*'}))
      ])
    ]),
    trigger('blurIn', [
      state('visible', style({filter: '*'})),
      transition('void => *', [
        style({filter: 'blur(3px)'}),
        animate("1000ms 500ms", style({filter: '*'}))
      ])
    ]),
    trigger('expandHeight', [
      state('collapsed', style({
        height: 0
            })),
      state('expanded', style({height: '*'})),
      transition('collapsed <=> expanded', [
        animate('750ms')
      ])
    ])
  ]
})
export class HeaderComponent implements OnInit {

  public resumeAnimate: string = "collapsed";
  public logo: Logo;
  @ViewChild('header-img-anchor') headerImg: HTMLDivElement;

  constructor(private _router: Router,
              private _navbarSettings: NavbarSettingsService,
              private _logoAnimation: LogoAnimationService)
  {
    this._navbarSettings.fontColor = 'lightgrey';
  }


  ngOnInit() {
    this._logoAnimation.appendLogo((document.getElementById('header-img-anchor') as HTMLDivElement)).then((logo) => {
      this.logo = logo;
      this._logoAnimation.animateLogo(logo, ELogoAnimations.BASIC, false, {delay: 500})
    })

  }

  toggleResumePDF(): void {
    this.resumeAnimate = this.resumeAnimate === "expanded" ? "collapsed" : "expanded";
  }

  navigateToResume(ev: Event){
    if (this.resumeAnimate === "expanded"){
      this._router.navigate([''], { fragment: 'resume'});
    } else {
      this._router.navigate(['']);
    }
  }


}
