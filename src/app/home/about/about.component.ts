import {Component, OnInit, HostListener, ElementRef, ViewChild} from '@angular/core';
import {trigger, state, transition, style, animate} from "@angular/animations";
import {ActivatedRoute} from "@angular/router";

/**
 * About section of Home Page.
 */
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [
    trigger('fadeIn', [
      state('active', style({opacity: '*'})),
      state('inactive', style({opacity: 0})),
      transition('inactive => active', [
        style({opacity: 0}),
        animate("750ms", style({opacity: '*'}))
      ])
    ]),
    trigger('blurIn', [
      state('focused', style({filter: '*', opacity: '*'})),
      state('blurred', style({filter: 'blur(1px)', opacity: .8})),
      transition('focused => blurred', [
        animate("10ms")
      ]),
      transition('blurred => focused', [
        animate("1000ms")

      ])
    ])
  ]
})
export class AboutComponent implements OnInit {


  public scrollFadeIn: string = 'inactive';
  public siteAboutFadeIn: string = 'inactive';
  public blurIn: string = 'focused';
  public scrolledIn: boolean = false;
  public targetOffsetTop: number;
  public siteAboutOffset: number;

  @ViewChild('siteAbout') siteAbout: ElementRef;

  /**
   * Event listener on document for scrolling that fades sections of this component based on scrollTop vs. Offset.
   * @param ev - event target
   */
  @HostListener('document:scroll', ['$event'])
  private onScroll(ev?) {

    const scrollTop = ev.target.scrollingElement.scrollTop;
    this.siteAboutOffset = this.siteAbout.nativeElement.offsetTop - 500;
    this.targetOffsetTop = this.el.nativeElement.offsetTop - 500;
    if (scrollTop >= this.targetOffsetTop) {

      this.scrolledIn = true;
      this.scrollFadeIn = 'active';
      this.blurIn = 'focused';
      if(scrollTop >= this.siteAboutOffset || scrollTop + this.siteAbout.nativeElement.clientHeight > ev.target.scrollingElement.clientHeight - 500){
        this.siteAboutFadeIn = 'active'
      }

    } else if(this.scrolledIn) {

      this.scrolledIn = false;
      this.blurIn = 'blurred';

    }

  }

  constructor(private el: ElementRef) {}

  ngOnInit() {

  }

}
