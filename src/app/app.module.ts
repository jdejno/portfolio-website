import { BrowserModule } from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {LocationStrategy, HashLocationStrategy, APP_BASE_HREF} from "@angular/common";
import {navbarRouting} from "./navbar/navbar.routes";
import { HomeComponent } from './home/home.component';
import {ProjectsModule} from "./projects/projects.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AboutComponent } from './home/about/about.component';
import { HeaderComponent } from './home/header/header.component';
import { BlogModule } from './blog/blog.module';
import {NavbarSettingsService} from "./navbar/navbar-settings.service";
import {CONSTANTS} from "../constants.service";
import {GitCodeService} from "./providers/git-code.service";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BlogModule,
    navbarRouting,
    ProjectsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: APP_BASE_HREF, useValue: '/'},
    {provide: NavbarSettingsService, useClass: NavbarSettingsService},
    GitCodeService,
    CONSTANTS
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
