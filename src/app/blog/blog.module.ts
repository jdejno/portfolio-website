import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule, APP_BASE_HREF} from '@angular/common';
import { BlogComponent } from './blog.component';
import { PostComponent } from './post/post.component';
import { PostService } from './post/post.service';
import {RouterModule} from "@angular/router";
import {blogRouting} from "./blog.routes";
import { PostThumbnailComponent } from './post-thumbnail/post-thumbnail.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    blogRouting
  ],
  exports: [
    BlogComponent,
    PostComponent
  ],
  declarations: [BlogComponent, PostComponent, PostThumbnailComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    PostService,
    {provide: APP_BASE_HREF, useValue: '/blog'}
  ]
})
export class BlogModule { }
