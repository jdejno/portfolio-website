import {Component, OnInit, Renderer2} from '@angular/core';
import {PostService, IPost} from "./post/post.service";
import {trigger, state, style, animate, transition} from "@angular/animations";
import {NavbarSettingsService} from "../navbar/navbar-settings.service";


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  animations: [
    trigger('collapsePost', [
      state('expanded', style({
        transform: '*',
        height: '*',
        opacity: '*'
      })),
      state('collapsed', style({
        transform: 'scale(0)',
        height: 0,
        opacity: 0
      })),
      transition('expanded <=> collapsed',
        animate("400ms")
      )
    ])
  ]
})
export class BlogComponent implements OnInit {

  private _posts: IPost[];
  public posts: IPost[];
  public categories: Set<string>;
  public filterCategories: Set<string> = new Set<string>();

  constructor(private postService: PostService,
              private render: Renderer2,
              private _navbarSettings: NavbarSettingsService)
  {
    this.categories = new Set<string>();
    this._navbarSettings.fontColor = '#df691a';
  }

  ngOnInit() {

    this.postService.getPosts().then((res) => {
      this.posts = res.map(d => {
        d.expanded = 'expanded';
        return d;
      });
      this._posts = this.posts.slice(0);
      this.categories = PostService.extractCategories(this.posts);
    });
  }

  public filterPostsByCat(ev: Event){
    this.filterCategories.add((ev.target as HTMLAnchorElement).getAttribute('data-category'));
    this.posts = this._getFilteredPosts();
  }

  public clearPostCategoryFilter(ev: Event){
    this.filterCategories.delete((ev.target as HTMLAnchorElement).getAttribute('data-category'));
    this.posts = this._getFilteredPosts();
  }

  private _getFilteredPosts(): IPost[] {
    //if no filterCats, return all the posts
    if(this.filterCategories.size === 0){
      return this._posts.map(d => {
        d.expanded = 'expanded';
        return d;
      });
    }
    //loop through posts and see if each post has categories that are currently selected in filterCategories.
    return this._posts.map(d => {
      for(let i in d.categories){
        if(this.filterCategories.has(d.categories[i])){
          d.expanded = 'expanded';
          return d;
        }
      }
      d.expanded = 'collapsed';
      return d;

    });
  }

}
