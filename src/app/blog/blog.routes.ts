import {Routes, RouterModule} from "@angular/router";
import {PostComponent} from "./post/post.component";
import {BlogComponent} from "./blog.component";
/**
 * Created by jeremydejno on 8/24/17.
 */


//Child routing to render a post based on the postId.
const routes: Routes = [
  {path: 'blog',
    component: BlogComponent,
    children: [
      {path: 'post/:postId', component: PostComponent}
    ]
  }
];

export const blogRouting = RouterModule.forChild(routes);
