import { Injectable } from '@angular/core';
import {Http, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import 'rxjs';


export interface IPost {
  id: number,
  title: string,
  date: Date,
  snippet: string,
  content: string,
  categories?: string[];
  imgUrl?: string,
  author?: string,
  expanded?: string
}

/**
 * Provide methods for accessing blog posts.
 */
@Injectable()
export class PostService {

  private _cachedPosts: IPost[];

  constructor(private http: Http) { }

  /**
   * Get blog posts from server and return as an array of json
   * @param {number} [startNum] - what blog post to start at for returning and array of posts [default: 0]
   * @param {number} [numOfPosts] - number of posts to return starting from the startNum
   * @returns {Promise<IPost>} - Async promise with data being array of blog posts objects.
   */
  public getPosts(startNum: number = 0, numOfPosts?: number) : Promise<IPost[]> {

    return new Promise((res, rej) => {
      //If posts are cached for this session, don't need to make another request.
      if(this._cachedPosts){
        res(this._cachedPosts);
        return;
      }

      this.http.get("/api/blog/posts").map(resp => resp.json()).subscribe(data => {

        this._cachedPosts = data.map(d => {
          return {
            id: d.id,
            title: d.title,
            snippet: d.snippet.substring(0, 170) + (d.snippet.length > 170 ? "..." : ""),
            date: new Date(d.date),
            categories: d.categories,
            imgUrl: d.imgUrl
          }
        });

        res(this._cachedPosts);

      });
    });
  }

  /**
   * Get a single blog post
   * @param {number} postId - blog post ID
   * @returns {Promise<IPost>} - Async promise with data being blog post object.
   */
  public getPost(postId: number): Promise<IPost> {

    let params = new URLSearchParams();
    params.set("post", postId.toLocaleString());

    let requestOptions = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});
    requestOptions.params = params;

    let postUrl = encodeURI('/api/blog/posts');

    return new Promise((res, rej) => {
      this.http.get(postUrl, requestOptions).map(resp => resp.json()).subscribe(data => {
        if(!data) {
          rej("No post for postId " + postId);
          return;
        }
        data.snippet = data.snippet.substring(0,170) + (data.snippet.length > 170 ? "..." : "");
        data.date = new Date(data.date);
        res(data);
      })
    });
  }

  static extractCategories(posts: IPost[]): Set<string>{
    let cats = new Set<string>();
    for(let i in posts) {
      posts[i].categories.forEach((d, i) => {
        cats.add(d);
      })
    }
    return cats;
  }

}
