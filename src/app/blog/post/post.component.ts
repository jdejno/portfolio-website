import {Component, OnInit, Renderer2, OnDestroy} from '@angular/core';
import {PostService} from "./post.service";
import {ActivatedRoute, Router} from "@angular/router";
import {trigger, style, state, transition, animate} from "@angular/animations";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  animations: [
    trigger('expand', [
      state('collapsed', style({height: 0, opacity: 0})),
      state('expanded', style({height: '100%', opacity: 1})),
      transition('void => expanded', [
        animate("750ms ease-out")
      ]),
      transition('* => collapsed', [
        animate("400ms")
      ])
    ])
  ]
})
export class PostComponent implements OnInit, OnDestroy {

  public post: any;
  public postId: number;
  public expanded: string = "collapsed";

  constructor(private postService: PostService,
              private route: ActivatedRoute,
              private router: Router,
              private render: Renderer2) {

  }

  ngOnInit() {
    //disable scrolling of body when post is initiated. Post element is scrollable.
    let body = document.getElementsByTagName('body')[0];
    this.render.setStyle(body, "overflow", "hidden");

    this.route.params.subscribe(p => {
      this.postId = +p.postId;
      this.postService.getPost(this.postId).then(res => {
        this.post = res;
        this.expanded = "expanded";
      })
    });

  }

  collapse() {
    this.expanded = "collapsed";

    this._enableBodyScroll();

    setTimeout(() => {
      this.router.navigate(["blog"]);
    }, 400); //sync with animation
  }

  ngOnDestroy() {

    this._enableBodyScroll();

  }

  private _enableBodyScroll() {
    let body = document.getElementsByTagName('body')[0];
    this.render.setStyle(body, "overflow", "auto");
  }

}
