import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinearTimeLineComponent } from './linear-time-line/linear-time-line.component';
import { PlaygroundComponent } from './playground/playground.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LinearTimeLineComponent, PlaygroundComponent],
  exports: [
    PlaygroundComponent,
    LinearTimeLineComponent
  ]
})
export class D3Module { }
