import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlaygroundComponent implements OnInit {

  private _margin = {left: 20, right: 20, top: 20, bottom: 20};

  public svgContainer;
  public svgHeight = 500;
  public svgWidth = 700;

  public width = +this.svgWidth - this._margin.left - this._margin.right;
  public height = +this.svgHeight - this._margin.top - this._margin.bottom;

  public g;

  public x;
  public y;

  public data;

  constructor() {
    this.data = this.getData();

    //get x and y values
    this.x = d3.scaleBand().rangeRound([0, this.width]).paddingInner(.05);
    this.y = d3.scaleLinear().rangeRound([this.height, 0]);

    this.x = this.x.domain(this.data.map(d => { return d['key'] }));
    this.y = this.y.domain([0, d3.max(this.data, d => { return d['value'] })])
  }

  ngOnInit() {

    this.svgContainer = d3.select('svg')
      .attr('height', this.svgHeight)
      .attr('width', this.svgWidth)
      .attr("class", "svg-container");

    //add graph g element
    this.g = this.svgContainer.append("g")
      .attr("transform", `translate(${this._margin.left}, ${this._margin.top})`);

    //add x axis
    this.g.append("g")
      .attr("class", "axis x")
      .attr("transform", `translate(0, ${this.height})`)
      .call(d3.axisBottom(this.x));

    //add y axis
    this.g.append("g")
      .attr("class", "axis y")
      .attr("height", this.height)
      .call(d3.axisLeft(this.y));

    this.g.selectAll(".bar")
      .data(this.data)
      .enter().append("rect")
      .attr("x", (d) => this.x(d.key))
      .attr("y", d => this.y(d.value))
      .attr("height", d => { return this.height - this.y(d.value); })
      .attr("width", this.x.bandwidth())
      .attr("class", "bar");

  }

  private getData(n?): any {
    return [
      { key: "A", value: 10 },
      { key: "B", value: 20 }
    ]
  }

}
