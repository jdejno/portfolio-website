import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinearTimeLineComponent } from './linear-time-line.component';
import {LifelineService} from "./lifeline.service";

describe('LinearTimeLineComponent', () => {
  let component: LinearTimeLineComponent;
  let fixture: ComponentFixture<LinearTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinearTimeLineComponent ],
      providers: [ LifelineService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinearTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
