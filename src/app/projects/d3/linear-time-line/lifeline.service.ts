import { Injectable } from '@angular/core';
import * as d3 from 'd3';

export type LinearTimeLineData = { dateTime: Date, title: string | number, category: string, tone: string }

export enum LinearTimeLineCategories {
  Life = 1,
  Work = 2,
  Social = 3
}

export enum LinearTimeLineTone {
  Happy = 1,
  Challenging = 2
}

@Injectable()
export class LifelineService {

  constructor() { }

  public getData(numDataPoints?: number): Promise<LinearTimeLineData[]> {

      return new Promise((res, rej) => {
        let ret: LinearTimeLineData[] = [];
        d3.json('/api/projects/data/lifeline', (err, data: any[]) => {
          if(err !== null && err !== undefined) {
            rej("Failed to load data for Life Line");
            return;
          }
          data.forEach((d, i) => {
            d.dateTime = Date.parse(d.dateTime);
            ret.push(d);
          });
          if(numDataPoints !== undefined) {
            res(ret.splice(0,numDataPoints));
          } else {
            res(ret);
          }

        });
      });

  }

  public saveData(data: LinearTimeLineData[]) {

  }

  //#region static methods

  public getDefaultData(title: string, date: Date): LinearTimeLineData {

    return {
      dateTime: date || new Date(Date.now()),
      title: title,
      category: LinearTimeLineCategories[LinearTimeLineCategories.Life],
      tone: LinearTimeLineTone[LinearTimeLineTone.Happy]
    }

  }


  //#endregion

}


