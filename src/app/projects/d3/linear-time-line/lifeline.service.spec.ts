import {TestBed, inject, async} from '@angular/core/testing';
import { LifelineService } from './lifeline.service';

describe('LifelineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LifelineService]
    });
  });

  it('should be created', inject([LifelineService], (service: LifelineService) => {
    expect(service).toBeTruthy();
  }));

  describe('LifelineService.getData', () => {
    let data;
    let dataPoint;

    beforeEach(async(inject([LifelineService], (service: LifelineService) => {
      service.getData(30).then(res => {
        data = res;
        dataPoint = data[0];
      });

    })));

    it('should return 30 data objects', inject([LifelineService], (service: LifelineService) => {
      expect(data.length).toBe(30);
    }));

    it('should return objects containing "dateTime" and "title" key', inject([LifelineService], (service: LifelineService) => {
      expect(typeof dataPoint).toBe(typeof jasmine.any(Object));
      expect(Object.keys(dataPoint)).toEqual(jasmine.arrayContaining([
        "title",
        "dateTime"
      ]));
    }));

    it(`should return objects with a Date for "dateTime" keys`, inject([LifelineService], (service: LifelineService) => {
      expect(typeof dataPoint["dateTime"]).toBe(typeof jasmine.any(Date));
    }));

    it('should return objects with a Number for "title" key', inject([LifelineService], (service: LifelineService) => {
      expect(typeof dataPoint["title"]).toBe(typeof "Hi");
    }));


  });

});
