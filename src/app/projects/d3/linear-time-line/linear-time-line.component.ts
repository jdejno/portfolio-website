import {Component, ViewEncapsulation, Input, OnChanges, SimpleChanges, ElementRef} from '@angular/core';
import * as d3 from 'd3';
import {LinearTimeLineData, LinearTimeLineCategories, LinearTimeLineTone} from "./lifeline.service";

@Component({
  selector: 'app-linear-time-line',
  templateUrl: 'linear-time-line.component.html',
  styleUrls: ['linear-time-line.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LinearTimeLineComponent implements OnChanges {

  //#region public properties

  public selection;
  public categories: string[] = [];
  public tones: string[] = [];
  @Input() data: LinearTimeLineData[];
  @Input() transpose: boolean;
  @Input() chartHeight: number = 500; //default to 500

  //#endregion

  //#region private properties

  private _svg;
  private _dataG;
  private _nodes;
  private _xAxisG;
  private _yAxisG;

  private _deleteBucket;
  private _deleteBucketContainer;
  private _tooltip;
  private _trackerDot;

  //#region d3 properties

  private x; //x domain - horizontal [dateTime]
  private y; //y domain - vertical [category]
  private z; //z domain - colors

  private timeFormat;
  private fullTimeFormat;

  //#endregion

  //#region UI properties

  private padding = {left: 150, top: 50, right: 0, bottom: 0};
  private svgYOffset;
  private height;
  private width;
  private containerHeight;
  private containerWidth;

  //#endregion

  //#region private flags

  private _initiated: boolean = false;

  //#endregion

  //#endregion

  constructor(private elRef: ElementRef) {}

  ngOnChanges(changes: SimpleChanges) {

    this.load(this.data as LinearTimeLineData[]);

  }

  //#region public members

  /**
   * load data into the graph
   * @param {LinearTimeLineData[]} data - data to be loaded into graph
   */
  public load(data: LinearTimeLineData[]) {

    this.data = data || [];
    this._load();
    this._initiated = true;

  }


  //#endregion

  //#region private members

  private _load() {
    this.init();
    this.draw();
    this.update();

  }

  //#region init

  /**
   * Initiate graph formats, scales, and elements. Will only add elements on first call.
   */
  private init()  {

    if(!this._initiated) {

      this.createMainElements();
      window.addEventListener('resize', () => this._load());

    }

    this.initFormats();
    this.initScales(this.padding);

  }

  /**
   * Initiate the formats used by the graph. dateTime formats and category/tone arrays for axes
   */
  private initFormats() {
    //global time formatting
    this.timeFormat = d3.timeFormat("%m/%y");
    this.fullTimeFormat = d3.timeFormat("%m/%d/%Y");

    //grab array of category and tone values for y/z scales
    this.categories = [];
    this.tones =[];
    if(this.data !== undefined && this.data !== null) {
      this.data.forEach(d => {
        if(this.categories.indexOf(d.category) < 0) {
          this.categories.push(d.category)
        }
        if(this.tones.indexOf(d.tone) < 0) {
          this.tones.push(d.tone)
        }
      });
    }


  }

  /**
   * Creates the main svg element and tooltip
   */
  private createMainElements() {

    //select the containing host element to append svg elements too
    this.selection = d3.select("#linear-time-line-chart");

    //add main svg container to append d3 elements
    this._svg = this.selection.append("svg")
      .attr("class", "svg-container")
      .attr("height", this.chartHeight)
      .attr("width", "100%");

    //get actual heights and widths from svg container and calculate padding
    this.height = this.selection.node().clientHeight - this.padding.top - this.padding.bottom;
    this.width = this.selection.node().clientWidth - this.padding.left - this.padding.right;
    this.svgYOffset = this.elRef.nativeElement.offsetTop;

    //create tooltip for hovering
    this._tooltip = d3.select("body").append("div")
      .attr("class", "tooltip timeline")
      .style("display", "none")
      .style("opacity", 0);

    //create delete bucket for dragging delete
    this._deleteBucketContainer = this._svg.append("g")
      .attr("transform", `translate(${this.width}, 0)`);

    this._deleteBucket = this._deleteBucketContainer
      .append("g")
      .style("fill-opacity", 0)
      .style("stroke-opacity", 0);


    this._deleteBucket.append("rect")
      .attr("class", "rect delete-bucket")
      .attr("height", "50")
      .attr("width", "50")
      .attr("ry", 2)
      .attr("rx", 30);


    this._deleteBucket.append("text")
      .attr("class", "text delete-bucket")
      .attr("y", this._deleteBucket.select("rect").attr("height") / 2)
      .attr("x", this._deleteBucket.select("rect").attr("width") / 2)
      .text("Delete").raise();

    this._deleteBucket.transition().duration(1000).attr("transform", "translate(0,0)")

    this._trackerDot = this._svg.append('circle')
      .attr("class", "tracker timeline")
      .attr("r", 5)
      .style("opacity", 0);

  }

  private initScales(padding: {top: number, left: number, bottom: number, right: number}) {

    //x domain in time scale
    this.x = d3.scaleTime().range([padding.left, this.width]).domain(this.data ? d3.extent(this.data, d => d.dateTime) : [0, 0]);

    //y domain in ordinal scale for life categories
    this.y = d3.scaleOrdinal().range(d3.range(padding.top, this.height, this.height / (this.categories.length || 1))).domain(this.categories);

    //z domain in ordinal scale using colors for tone of life events
    this.z = d3.scaleOrdinal().range(d3.schemeCategory10).domain(this.tones);

  }

  /**
   * Calculate the axes scales from this.data
   * @param {object} padding - object containing all four padding values for the graph
   */
  private updateScales(padding: {top: number, left: number, bottom: number, right: number}) {

    let [yDomainData, zDomainData] = this.transpose ? [this.tones, this.categories] : [this.categories, this.tones];

    this.x.domain(this.data ? d3.extent(this.data, d => d.dateTime) : [0, 0]);

    this.y.range(d3.range(padding.top, this.height, this.height / (yDomainData.length || 1))).domain(yDomainData);

    this.z.domain(zDomainData);

  }

  //#endregion init

  //#region draw

  /**
   * Draw axes and data
   */
  private draw() {

    //get actual heights and widths from container and calculate padding on draw
    this.height = this.selection.node().clientHeight - this.padding.top - this.padding.bottom;
    this.width = this.selection.node().clientWidth - this.padding.left - this.padding.right;

    this.updateScales(this.padding);
    this.drawXAxis();
    this.drawYAxis();
    this.drawGroupContainer();
    this.resize();

  }

  private resize() {

    this.containerHeight = this.selection.node().clientHeight;
    this.containerWidth = this.selection.node().clientWidth;

    //console.log(`Height: ${this.containerHeight} Width: ${this.containerWidth}`);
    //add main svg container to append d3 elements
    this._svg
      .attr("height", this.containerHeight)
      .attr("width", this.containerWidth);

    this._deleteBucketContainer
      .attr("transform", `translate(${this.width}, 0)`)
  }

  /**
   * Draw the xAxis and cache in this._xAxis
   */
  private drawXAxis() {

    if(!this._initiated) {
      this._xAxisG = this._svg.append("g");
    }
    let ticks = this.width > 700 ? 10 : this.width > 550 ? 5 : 3;


    let axis = d3.axisBottom(this.x)
      .ticks(ticks)
      .tickFormat(this.timeFormat)
      .tickSize(10);

    this._xAxisG
      .attr("class", "x axis timeline")
      .attr("transform", `translate(0, ${this.height})`)
      .transition()
      .duration(750)
      .call(axis);

  }

  /**
   * Draw yAxis and cache in this._yAxis
   */
  private drawYAxis() {

    if(!this._initiated) {
      this._yAxisG = this._svg.append("g");
    }

    let axis = d3.axisLeft(this.y);

    this._yAxisG
      .attr("class", "y axis timeline")
      .attr("transform", `translate(${this.padding.left}, 0)`)
      .transition()
      .duration(750)
      .call(axis);

    this.updateScales(this.padding);

  }

  /**
   * Draw main data group containers. Only created on first call (!this._initiated)
   */
  private drawGroupContainer() {

    if(!this._initiated) {

      this._dataG = this._svg.append("g");
    }

  }

  //#endregion

  //#region update

  /**
   * Update data in graph from this.data
   */
  private update() {

    this.updateCircles();

  }

  /**
   * Update circle/nodes in graph with this.data
   */
  private updateCircles() {

    //add data to nodes
    this._nodes = this._dataG.selectAll(".life-point")
      .data(this.data);

    let circles;
    if(this.data) {
      //merge the new and old nodes together and cache
      circles = this._nodes
        .enter().append("circle")
        .attr("class", "life-point active")
        .attr("r", 5)
        .style("stroke", "grey")
        .style("fill-opacity", .5)
        .attr("cx", this.width / 2)
        .attr("cy", this.height / 2)
        .merge(this._nodes)
        .on("mouseover", d => this.circleOnEventHandler(d))
        .on("mouseout", d => this.circleOffEventHandler(d))
        .on("click", d => this.circleClickEventHandler(d))
        .call(d3.drag()
          .on("start", this.dragStarted())
          .on("drag", this.dragging())
          .on("end", this.dragEnded())
        );

    }

    this.transitionCircles(circles, this._nodes.exit());


  }

  //#region transitions

  /**
   * Execute transitions for entering and exiting data elements
   * @param enterCircles - new data elements entering the data group
   * @param exitCircles - elements to be removed from data group (no more data associated with them)
   */
  private transitionCircles(enterCircles, exitCircles) {

    this.enterCircles(enterCircles);
    this.exitCircles(exitCircles);

  }

  private enterCircles(circles) {

    circles.transition()
      .duration(500)
      .delay(d => this.y(this.transpose ? d.tone as LinearTimeLineTone : d.category as LinearTimeLineCategories) * 2)
      .style("fill-opacity", 0)
      .attr("cx", d => this.x(d.dateTime as Date))
      .transition()
      .duration(500)
      .attr("cy", d  => this.y(this.transpose ? d.tone as LinearTimeLineTone : d.category as LinearTimeLineCategories))
      .style("stroke", d => this.z(this.transpose ? d.category as LinearTimeLineCategories : d.tone as LinearTimeLineTone));

  }

  private exitCircles(circles) {

    circles.transition()
      .duration(1000)
      .attr("cy", this.height)
      .style("opacity", 0)
      .remove()

  }


  //#endregion

  //#endregion

  //#region event handlers

  /**
   * Show tooltip on mouseover event
   * @param d - datum
   */
  private circleOnEventHandler(d) {

    this._tooltip.transition()
      .duration(200)
      .style("display", "block")
      .style("opacity", .7);

    this.updateTooltip(d, [d3.event.pageX, d3.event.pageY]);

    d3.select(d3.event.currentTarget).attr("r", 10);

  }

  /**
   * Hide tooltip on mouseoff event
   * @param d -  datum
   */
  private circleOffEventHandler(d) {

    this._tooltip.transition()
      .duration(200)
      .style("display", "none")
      .style("opacity", 0);

    d3.select(d3.event.currentTarget).attr("r", 5);

  }

  /**
   * Change color and zDomain value for a click event on a circle
   * @param d
   */
  private circleClickEventHandler(d) {

    let nextToneIndex = this.tones.lastIndexOf(d.tone) + 1,
      nextCatIndex = this.categories.lastIndexOf(d.category) + 1;
    if(nextToneIndex === this.tones.length) {
      nextToneIndex = 0;
    }
    if(nextCatIndex === this.categories.length){
      nextCatIndex = 0;
    }

    d3.select(d3.event.currentTarget)
      .style("stroke", this.z(this.transpose ? d.category = this.categories[nextCatIndex] : d.tone = this.tones[nextToneIndex]));
    this.updateTooltip(d, [d3.event.pageX, d3.event.pageY])

  }

  /**
   * Update tooltip position and display data
   * @param d - datum
   * @param coords - x and y coordinates for tooltip position
   */
  private updateTooltip(d, coords: number[]) {

    let [x, y] = coords;

    this._tooltip.html(`${d.title}<br />${this.fullTimeFormat(d.dateTime)}<br />${this.transpose ? d.category : d.tone}`)
      .style("display", "block")
      .style("left", `${x - ( this._tooltip.node().clientWidth / 2)}px`)
      .style("top", `${y + 20}px`)
      .style("background-color", this.z(this.transpose ? d.category : d.tone));
  }

  //#endregion

  //#region drag event handlers

  /**
   * Handle dragstart event by raising circle element and adding 'active' class
   *
   */
  private dragStarted() {
    let self = this;
    return function(d, i) {
      d3.select(this)
        .raise()
        .classed("active", true);

      self._deleteBucket
        .transition()
        .style("fill-opacity", .75)
        .style("stroke-opacity", .75)
        .transition()
        .attr("transform", `scale(1.2)`)
    };

  }

  /**
   * Handle dragging event by updating circle position and tooltip
   */
  private dragging() {
    //console.log(`Cat: ${Math.round(d3.event.y / (this.height / this.y.domain().length))}`)
    let self = this;
    return function(d,i) {
      let select = d3.select(this);
      select
        .attr("cx", d3.event.x)
        .attr("cy", d3.event.y);

      d.dateTime = self.x.invert(d3.event.x);

      let x = +select.attr("cx"),
        y = +select.attr("cy");

      self.updateTooltip(d, [x, y + self.svgYOffset]);

      //use tracker dot to see where lifepoint will snap to
      let [date, cat] = self.snapCategoryHelper([x, y]);
      self._trackerDot
        .style("opacity", .7)
        .attr("cx", self.x(date))
        .attr("cy", self.y(cat));

      if(self.inDeleteBucket([x, y])) {
        self._deleteBucket.selectAll("rect")
          .style("fill", "red");
      }else {
        self._deleteBucket.selectAll("rect")
          .style("fill", "none");
      }
    };

  }

  /**
   * Handle drag ended event by snapping y position to an yAxis tick and updating circle datum values
   * @param d
   * @param i
   */
  private dragEnded() {
    let self = this;

    return function(d, i) {

      if (self.dragDelete(i, [d3.event.x, d3.event.y])) return;

      let [x, y] = self.snapCategoryHelper([d3.event.x, d3.event.y]);
      d3.select(this)
        .classed("active", false)
        .attr("cx", self.x(d.dateTime = x))
        .attr("cy", self.y(self.transpose ? d.tone = y : d.category = y));

      self.updateTooltip(d, [d3.event.pageX, self.y(y) + self.svgYOffset]);
      self._trackerDot.style("opacity", 0);
    };

  }

  private dragDelete(index: number, coords: [number, number]): boolean {

    let ret = false;

    if(this.inDeleteBucket(coords)) {

      this.data.splice(index, 1);
      this.load(this.data);
      ret = true;

    }

    this._deleteBucket.selectAll("rect")
      .style("fill", "none");

    this._deleteBucket
      .transition()
      .style("fill", "none")
      .style("fill-opacity", 0)
      .style("stroke-opacity", 0)
      .attr("transform", `scale(1)`);

    this._trackerDot.style("opacity", 0);
    this._tooltip.style("opacity", 0);

    return ret;

  }

  private inDeleteBucket(coords: [number, number]): boolean {

    let [x, y] = coords,
      x0 = this.width,
      x1 = this.width + +this._deleteBucket.select("rect").attr("width"),
      y0 = 0,
      y1 = this._deleteBucket.select("rect").attr("height");

    return (x > x0 && x < x1 && y < y1);

  }

  //#endregion

  //#region helpers
  /**
   * Return xDomain and yDomain datum values from x and y coordinates
   * @param coords - array of x and y coordinates
   * @returns {[Date|number|number[]|null,any]}
   */
  private snapCategoryHelper(coords: number[]): any[] {
    let [x, y] = coords,
      sectionY = this.height / this.y.domain().length,
      index = (y / sectionY) >= this.y.domain().length ? this.y.domain().length - 1 : Math.round(y / sectionY) - 1,
      cat = this.y.domain()[index],
      date = this.x.invert(x);
    return [date, cat]

  }

  //#endregion

  //#endregion


}
