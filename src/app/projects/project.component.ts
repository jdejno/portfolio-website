import { Component, OnInit } from '@angular/core';
import {IProject, ProjectService} from "./providers/project.service";


/**
 * Lists all projects in a grid layout with title, image, and description.
 */
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {



  public projects: IProject[];

  constructor(private _projectService: ProjectService) {

  }

  ngOnInit() {
    this._projectService.getAllProjects().then((res: IProject[]) => {
      this.projects = res;
    });
  }

}
