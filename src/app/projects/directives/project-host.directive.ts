import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appProjectHost]'
})
export class ProjectHostDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
