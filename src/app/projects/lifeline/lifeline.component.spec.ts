import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifelineComponent } from './lifeline.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";

describe('LifelineComponent', () => {
  let component: LifelineComponent;
  let fixture: ComponentFixture<LifelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifelineComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
