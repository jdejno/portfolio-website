import {Component, OnInit} from '@angular/core';
import $ from "jquery";
import "../../../../bower_components/smalot-bootstrap-datetimepicker"
import {LifelineService, LinearTimeLineData} from "../d3/linear-time-line/lifeline.service";
import {trigger, state, style, transition, animate} from "@angular/animations";

@Component({
  selector: 'app-lifeline',
  templateUrl: './lifeline.component.html',
  styleUrls: ['./lifeline.component.scss'],
  providers: [LifelineService],
  animations:[
    trigger('expand', [
      state('collapsed', style({
        height: 0,
        border: 0,
        padding: 0
      })),
      state('expanded', style({
        height: '*',
        border: '*',
        padding: '*'
      })),
      transition('collapsed <=> expanded', [
        animate('1000ms ease-out')
      ])
    ])
  ]
})
export class LifelineComponent implements OnInit {

  //#region properties

  //#region data properties
  public data: LinearTimeLineData[];
  public transpose: boolean = false;
  private tempData: LinearTimeLineData[];
  public dateTime: Date;
  //#endregion

  public codeExpand: string = 'collapsed';
  public tooltipContent: string = 'The goods...';

  //#endregion


  constructor(private _lifeLineService: LifelineService) {}

  ngOnInit() {

    this.init();

    this._lifeLineService.getData().then(res => {
      res = res.map(d => {
        d.dateTime = new Date(d.dateTime);
        return d;
      });


      this.data = [];
      //staggers data point entry into chart
      let intervalTime = 750;
      let dataPointsPerInt = 5;
      let int = setInterval(() => {
        if(res.length < 1) {
          clearInterval(int);
          return;
        }
        let temp = res.splice(0, (res.length < dataPointsPerInt ? res.length : dataPointsPerInt));
        this.data = this.data.concat(temp);
      }, intervalTime)

    });

  }

  static staggerData(data: Array<any>, datumPerInterval: number = 1, target: Array<any>) {


    setTimeout(() => {

      console.log(target);
      LifelineComponent.staggerData(data, datumPerInterval, target)
    }, 2000);

  }

  //#region private members

  private init() {

    this.initDatetimePicker();

  }

  private initDatetimePicker() {
    $('.form_datetime').datetimepicker({
      autoclose: true,
      pickerPosition: "bottom-left"
    })
      .on("changeDate", (ev) => {
        this.setDatetime(ev.date);
      })

  }

  //#region input controllers

  private setDatetime(dateInput: string) {

    this.dateTime = new Date(Date.parse(dateInput));

  }

  /**
   * submit a new event to the linear-timeline
   * @param title (string) - title of event to add
   */
  public submitEvent(title: string) {

      this.tempData = this.data.splice(0);
      this.tempData.push(this._lifeLineService.getDefaultData(title, this.dateTime));
      this.data = this.tempData;


    }

  public toggleCodeExpand(ev) {
    this.codeExpand = this.codeExpand === 'collapsed' ? 'expanded' : 'collapsed';
    //sync with animation.
    setTimeout(() => {
      this.tooltipContent = this.codeExpand === 'collapsed' ? 'The goods...' : 'Close the goods...';
    }, 1000);
  }

  //#endregion


  //#endregion

}
