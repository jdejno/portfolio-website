import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { LifelineComponent } from './lifeline/lifeline.component';
import { D3Module } from './d3/d3.module';
import {ProjectComponent} from "./project.component";
import {FormsModule} from "@angular/forms";
import { UsaMapComponent } from './usa-map/usa-map.component';
import { SingleProjectComponent } from './single-project/single-project.component';
import {ProjectService} from "./providers/project.service";
import { ProjectHostDirective } from './directives/project-host.directive';
import {router} from "./projects.routes";
import { BitwiseComponent } from './bitwise/bitwise.component';
import { HighlightExpressionDirective } from './bitwise/highlight-expression.directive';
import {CONSTANTS} from "../../constants.service";
import {LogoAnimationModule} from "./logo-animation/logo-animation.module";
import {LogoAnimationComponent} from "./logo-animation/logo-animation.component";

@NgModule({
  imports: [
    CommonModule,
    D3Module,
    FormsModule,
    router,
    LogoAnimationModule
  ],
  declarations: [
    LifelineComponent,
    ProjectComponent,
    UsaMapComponent,
    SingleProjectComponent,
    ProjectHostDirective,
    BitwiseComponent,
    HighlightExpressionDirective,
  ],
  providers:[
    ProjectService,
    CONSTANTS
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    LifelineComponent,
    UsaMapComponent,
    BitwiseComponent,
    LogoAnimationComponent
  ]
})
export class ProjectsModule { }

