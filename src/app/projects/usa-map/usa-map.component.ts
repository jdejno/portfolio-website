import {Component, OnInit, Input} from '@angular/core';
import * as d3 from 'd3';
import * as topojson from 'topojson';

@Component({
  selector: 'app-usa-map',
  templateUrl: './usa-map.component.html',
  styleUrls: ['./usa-map.component.scss']
})
export class UsaMapComponent implements OnInit {

  //#region public properties

  @Input() height: number = 500;
  public width: number;
  public padding: {left: number, top: number, right: number, bottom: number};

  //#endregion

  //#region private properties

  //SVG Elements
  public svg;
  public path;
  public node;

  //UI properties
  private group: any;
  private usa: any;
  private usaSubunits: any;
  private usaPath;
  private wisconsin;
  private wiPath;
  private lakes: any;
  private lakesPath: any;
  private wiPlaces: any;
  private midwest: any;
  private midwestPath: any;
  private northeast: any;
  private northeastPath: any;
  private south: any;
  private southPath: any;
  private west: any;
  private westPath: any;


  //#endregion

  constructor() {

  }

  ngOnInit() { this.init() }

  drawMapLines() {

    if(this.usaPath === undefined || this.wiPath === undefined) return;

    let drawWisconsin = () => {

      return this.wiPath
        .transition()
        .duration(1000)
        .ease(d3.easeLinear)
        .style('fill-opacity', '.95')
        .attr('stroke-dashoffset', 0);

    };

    let drawLakes = () => {
      return this.lakesPath
        .transition()
        .duration(750)
        .ease(d3.easeLinear)
        .style('fill', '#337ab7')
        .style('stroke', '#286090')
        .attr('stroke-dashoffset', 0);

    };

    let drawUSA = () => {

      return this.usaPath
        .transition()
        .duration(2000)
        .ease(d3.easeLinear)
        .style('stroke', 'black')
        .attr('stroke-dashoffset', 0);

    };

    let chainDrawRegions = (nextTransition) => {

      drawRegion(this.midwestPath, '#ddd')
        .on('end', () => {
          drawRegion(this.northeastPath, '#ddc')
            .on('end', () => {
              drawRegion(this.southPath, '#dcd')
                .on('end', () => {
                  drawRegion(this.westPath, '#cdc')
                    .on('end', () => {
                      fadeRegions(1500, d3.easeBackInOut).on('end', nextTransition);
                    })
                })
            })
        });

      let fadeRegions = (duration, ease)  => {

        this.usaPath.transition().duration(duration).ease(ease).style('fill-opacity', .8);
        this.midwestPath.transition().duration(duration).ease(ease).style('fill', 'transparent');
        this.northeastPath.transition().duration(duration).ease(ease).style('fill', 'transparent');
        this.southPath.transition().duration(duration).ease(ease).style('fill', 'transparent');
        return this.westPath.transition().duration(duration).ease(ease).style('fill', 'transparent')
      }
    };

    let drawRegion = (region, color?: string) => {
      return region
        .transition()
        .duration(750)
        .ease(d3.easeCircleIn)
        .style('fill', color || 'white')
        .attr('stroke-dashoffset', 0);
    };

    drawUSA()
      .on('end', () => {
        chainDrawRegions(drawWisconsin)
      });

    drawLakes();

  }

  public loadGeoData() {

    d3.json("/api/projects/data/usa-map", (error, usa) => {
      if (error) return console.log(error);
      this.usa = usa;
      this.drawMap(this.usa);

    });

  }


  public drawMap(topo: any) {

    this.usaSubunits = topojson.feature(topo, topo.objects.usa_sub);
    this.wisconsin = topojson.feature(topo, topo.objects.wisconsin);
    this.lakes = topojson.feature(topo, topo.objects.great_lakes);
    this.wiPlaces = topojson.feature(topo, topo.objects.wi_places);
    this.midwest = topojson.feature(topo, topo.objects.midwest);
    this.northeast  = topojson.feature(topo, topo.objects.northeast);
    this.south = topojson.feature(topo, topo.objects.south);
    this.west = topojson.feature(topo, topo.objects.west);

    let projection = d3.geoAlbersUsa().scale(600).translate([this.width / 2, this.height / 2]).precision(.1);

    this.usaPath = this.group.append("path")
      .attr("class", "usa topography")
      .datum(this.usaSubunits)
      .attr("d", d3.geoPath().projection(projection));

    this.lakesPath = this.group.append("path")
      .attr("class", "lakes topography")
      .datum(this.lakes)
      .attr("d", d3.geoPath().projection(projection));

    this.wiPath = this.group.append("path")
      .attr("class", "wi topography")
      .datum(this.wisconsin)
      .attr("d", d3.geoPath().projection(projection))
      .raise();

    this.midwestPath = this.group.append("path")
      .attr("class", "midwest region topography")
      .datum(this.midwest)
      .attr("d", d3.geoPath().projection(projection))
      .lower();

    this.northeastPath = this.group.append("path")
      .attr("class", "northeast region topography")
      .datum(this.northeast)
      .attr("d", d3.geoPath().projection(projection))
      .lower();

    this.southPath = this.group.append("path")
      .attr("class", "south region topography")
      .datum(this.south)
      .attr("d", d3.geoPath().projection(projection))
      .lower();

    this.westPath = this.group.append("path")
      .attr("class", "west region topography")
      .datum(this.west)
      .attr("d", d3.geoPath().projection(projection))
      .lower();

    UsaMapComponent.setDashOffsetToZero(this.usaPath);
    UsaMapComponent.setDashOffsetToZero(this.wiPath);
    UsaMapComponent.setDashOffsetToZero(this.southPath);
    UsaMapComponent.setDashOffsetToZero(this.westPath);
    UsaMapComponent.setDashOffsetToZero(this.midwestPath);
    UsaMapComponent.setDashOffsetToZero(this.northeastPath);

    this.drawMapLines();

  }

  private init() {

    this.initSVGElement();
    this.loadGeoData();

  }

  /*
   Initiate the main SVG element
   */
  private initSVGElement() {

    this.svg = d3.select('#d3-usa-map').append('svg')
      .attr('height', this.height)
      .style('fill', "transparent")
      .attr('width', '100%');

    this.width = this.svg.node().clientWidth;
    this.padding = {left: 20, top: 20, right: 20, bottom: 20};

    this.group = this.svg.append('g')
      .style('transform', `translate(${this.padding.left}px, ${this.padding.top}px)`)
      .attr('height', this.height - this.padding.bottom)
      .attr('width', this.width - this.padding.right);

    window.addEventListener("resize", () => this.resizeHandling())
  }

  static setDashOffsetToZero(path) {
    let pathLength = path.node().getTotalLength();
    path
      .attr('stroke-dasharray', `${pathLength} ${pathLength}`)
      .attr('stroke-dashoffset', pathLength);
  }

  private resizeHandling(): void{
    this.width = this.svg.node().clientWidth;
    debugger;
    let groupWidth = this.group.node().clientWidth;
    console.log(groupWidth)
    this.group
      .style('transform', `translate(${this.width - groupWidth - this.padding.left}px, ${this.padding.top}px)`)

  }

}
