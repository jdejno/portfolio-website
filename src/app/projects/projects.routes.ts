import {Routes, RouterModule} from "@angular/router";
import {SingleProjectComponent} from "./single-project/single-project.component";
/**
 * Created by jeremydejno on 9/1/17.
 */


const routes: Routes = [
  {path: 'projects/project/:projectID', component: SingleProjectComponent}
];

export const router = RouterModule.forChild(routes);
