import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appHighlightExpression]'
})
export class HighlightExpressionDirective implements OnInit{

  constructor(private el: ElementRef) {

  }

  ngOnInit() {
    this.el.nativeElement.style.backgroundColor = "yellow";
    this.el.nativeElement.style.fontWeight = "bold";


  }



}
