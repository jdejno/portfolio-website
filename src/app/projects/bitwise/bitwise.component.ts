import { Component, OnInit } from '@angular/core';
import * as mathParser from './math.js';
import * as nearley from 'nearley';



@Component({
  selector: 'app-bitwise',
  templateUrl: './bitwise.component.html',
  styleUrls: ['./bitwise.component.scss']
})
export class BitwiseComponent implements OnInit {

  parser;
  nearley;
  curExpression: string;
  public parsedResults: any[];

  constructor() {
      this.nearley = nearley;
      this.parser = new this.nearley.Parser(nearley.Grammar.fromCompiled(mathParser));
  }

  ngOnInit() {
  }

  resetParser(){
    mathParser.operationsArr = [];
    this.parser =  new this.nearley.Parser(nearley.Grammar.fromCompiled(mathParser));
  }

  onEnter(value: string) {

    this.curExpression = value;

    try{

      this.parser.feed(value);
      this.parseResults();

    } catch(e) {

      console.log(e);

    }
    this.resetParser();

  }

  parseResults() {

    let results = this.parser.results[0],
        final = results[results.length - 1],
        data = [];

    results = results.filter(d => d.resultNum === results[results.length - 1].resultNum);
    console.log(results);

    let concatCurExpression = (origString: string, subExp: string, location: number, subExpValue: string): string => {
      let startIndex = origString.indexOf(subExp, location);
      let left = origString.slice(0, startIndex),
        right = origString.slice(startIndex+subExp.length, origString.length);
      return left + subExpValue + right;
    };

    let getLeftCurExpression = (origString: string, subExp: string, location: number): string => {
      let startIndex = origString.indexOf(subExp, location);
      return origString.slice(0, startIndex).replace(/[()]*/g,"");
    };

    let getRightCurExpression = (origString: string, subExp: string, location: number): string => {
      let startIndex = origString.indexOf(subExp, location);
      return origString.slice(startIndex+subExp.length, origString.length).replace(/[()]*/g,"");
    };

    console.group("Results");
    for(let i in results){
      let r = results[i];

      let o = {
            index: +i + 1,
            expression: r.original,
            value: r.evaluated,
            bitString: r.evaluated.toString(2),
            hexaDecimal: r.evaluated.toString(16),
            leftCurExpression: getLeftCurExpression(final.original, r.original, r.location),
            rightCurExpression: getRightCurExpression(final.original, r.original, r.location),

      };

      console.log(o);
      data.push(o);
    }
    console.groupEnd();

    if(data.length === 0){
      data = [
        {
          index: 1,
          expression: this.curExpression,
          value: this.curExpression,
          hexaDecimal: parseFloat(this.curExpression).toString(16),
          bitString: parseFloat(this.curExpression).toString(2)
        }
      ]
    }
    this.parsedResults = data;
  }



}
