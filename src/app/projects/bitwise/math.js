"use strict";

// Generated automatically by nearley
// http://github.com/Hardmath123/nearley

(function () {
  function id(x) {
    return x[0];
  }

  var cnt = 0;
  var resultNum = 0;

  var join = function join(d) {
    if (d === null) {
      return null;
    }
    if (Array.isArray(d)) {
      return d.filter(function (d) {
        return d !== null;
      }).join('');
    }
    return d;
  };

  var process = function process(type, evalString, orig, noCount, location, indexCheck) {
    var order = type === "P" ? 1 : type === "E" ? 2 : type === "BD" ? 3 : type === "MD" ? 4 : type === "A" ? 5 : type === "S" ? 6 : 7;

    var o = {
      order: order,
      type: type,
      location: location,
      evaluated: evalString,
      original: join(orig),
      step: noCount ? cnt : cnt++
    };

    if (!noCount) {

      o.original = "(" + o.original + ")";
      grammar.operationsArr.push(o);
    }

    return o;
  };

  var getSortedArray = function getSortedArray(arr) {
    var full = arr[arr.length - 1];
    resultNum++;

    var i = 0;
    while (i < arr.length) {
      arr[i].resultNum = resultNum;
      if (!full.original.includes(arr[i].original)) {
        arr.splice(i, 1);
      }
      i++;
    }

    return arr;
  };
  var grammar = {
    operationsArr: [],
    Lexer: undefined,
    ParserRules: [{ "name": "main", "symbols": ["_", "AS", "_"], "postprocess": function postprocess(d, l) {
        return getSortedArray(grammar.operationsArr);
      } }, { "name": "P", "symbols": [{ "literal": "(" }, "_", "AS", "_", { "literal": ")" }], "postprocess": function postprocess(d, l) {
        return process("P", d[2].evaluated, [d[0], d[2].original, d[4]]);
      } }, { "name": "P", "symbols": ["N"], "postprocess": function postprocess(d, l) {
        return process("N", d[0].evaluated, d[0].original, true);
      } }, { "name": "E$string$1", "symbols": [{ "literal": "*" }, { "literal": "*" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "E", "symbols": ["P", "_", "E$string$1", "_", "E"], "postprocess": function postprocess(d, l) {
        return process("E", Math.pow(d[0].evaluated, d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "E", "symbols": ["P"], "postprocess": function postprocess(d, l) {
        return process("P", d[0].evaluated, d[0].original, true);
      } }, { "name": "BD$string$1", "symbols": [{ "literal": ">" }, { "literal": ">" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "BD", "symbols": ["BD", "_", "BD$string$1", "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD$string$2", "symbols": [{ "literal": "<" }, { "literal": "<" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "BD", "symbols": ["BD", "_", "BD$string$2", "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD$string$3", "symbols": [{ "literal": ">" }, { "literal": ">" }, { "literal": ">" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "BD", "symbols": ["BD", "_", "BD$string$3", "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD", "symbols": ["BD", "_", { "literal": "&" }, "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD", "symbols": ["BD", "_", { "literal": "|" }, "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD", "symbols": ["BD", "_", { "literal": "^" }, "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD", "symbols": ["BD", "_", { "literal": "&" }, "_", "E"], "postprocess": function postprocess(d, l) {
        return process("BD", eval(d[0].evaluated + " " + d[2] + " " + d[4].evaluated), [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "BD", "symbols": ["E"], "postprocess": function postprocess(d, l) {
        return process("E", d[0].evaluated, d[0].original, true);
      } }, { "name": "MD", "symbols": ["MD", "_", { "literal": "*" }, "_", "BD"], "postprocess": function postprocess(d, l) {
        return process("MD", d[0].evaluated * d[4].evaluated, [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "MD", "symbols": ["MD", "_", { "literal": "/" }, "_", "BD"], "postprocess": function postprocess(d, l) {
        return process("MD", d[0].evaluated / d[4].evaluated, [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "MD", "symbols": ["BD"], "postprocess": function postprocess(d, l) {
        return process("BD", d[0].evaluated, d[0].original, true);
      } }, { "name": "AS", "symbols": ["AS", "_", { "literal": "+" }, "_", "MD"], "postprocess": function postprocess(d, l) {
        return process("A", d[0].evaluated + d[4].evaluated, [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "AS", "symbols": ["AS", "_", { "literal": "-" }, "_", "MD"], "postprocess": function postprocess(d, l) {
        return process("S", d[0].evaluated - d[4].evaluated, [d[0].original, d[2], d[4].original], false, l);
      } }, { "name": "AS", "symbols": ["MD"], "postprocess": function postprocess(d, l) {
        return process("MD", d[0].evaluated, d[0].original, true);
      } }, { "name": "N", "symbols": ["float"], "postprocess": function postprocess(d, l) {
        return process("N", d[0].evaluated, d[0].original, true);
      } }, { "name": "N$string$1", "symbols": [{ "literal": "s" }, { "literal": "i" }, { "literal": "n" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$1", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.sin(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$2", "symbols": [{ "literal": "c" }, { "literal": "o" }, { "literal": "s" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$2", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.cos(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$3", "symbols": [{ "literal": "t" }, { "literal": "a" }, { "literal": "n" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$3", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.tan(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$4", "symbols": [{ "literal": "a" }, { "literal": "s" }, { "literal": "i" }, { "literal": "n" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$4", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.asin(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$5", "symbols": [{ "literal": "a" }, { "literal": "c" }, { "literal": "o" }, { "literal": "s" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$5", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.acos(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$6", "symbols": [{ "literal": "a" }, { "literal": "t" }, { "literal": "a" }, { "literal": "n" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$6", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.atan(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$7", "symbols": [{ "literal": "p" }, { "literal": "i" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$7"], "postprocess": function postprocess(d, l) {
        return process("N", Math.PI, Math.PI, true);
      } }, { "name": "N", "symbols": [{ "literal": "e" }], "postprocess": function postprocess(d, l) {
        return process("N", Math.E, Math.E, true);
      } }, { "name": "N$string$8", "symbols": [{ "literal": "s" }, { "literal": "q" }, { "literal": "r" }, { "literal": "t" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$8", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.sqrt(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$9", "symbols": [{ "literal": "l" }, { "literal": "n" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$9", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", Math.log(d[2].evaluated), [d[0], d[2].original]);
      } }, { "name": "N$string$10", "symbols": [{ "literal": "+" }, { "literal": "+" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$10", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", ++d[2].evaluated, [d[0], d[2].original]);
      } }, { "name": "N$string$11", "symbols": [{ "literal": "-" }, { "literal": "-" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["N$string$11", "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", --d[2].evaluated, [d[0], d[2].original]);
      } }, { "name": "N", "symbols": [{ "literal": "~" }, "_", "P"], "postprocess": function postprocess(d, l) {
        return process("N", ~d[2].evaluated, [d[0], d[2].original]);
      } }, { "name": "N$string$12", "symbols": [{ "literal": "+" }, { "literal": "+" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["P", "_", "N$string$12"], "postprocess": function postprocess(d, l) {
        return process("N", ++d[0].evaluated, [d[0].original, d[2]]);
      } }, { "name": "N$string$13", "symbols": [{ "literal": "-" }, { "literal": "-" }], "postprocess": function joiner(d) {
        return d.join('');
      } }, { "name": "N", "symbols": ["P", "_", "N$string$13"], "postprocess": function postprocess(d, l) {
        return process("N", --d[0].evaluated, [d[0].original, d[2]]);
      } }, { "name": "float", "symbols": ["int", { "literal": "." }, "int"], "postprocess": function postprocess(d, l) {
        return process("float", parseFloat(d[0].evaluated + d[1] + d[2].evaluated), parseFloat(d[0].original + d[1] + d[2].original), true);
      } }, { "name": "float", "symbols": ["int"], "postprocess": function postprocess(d, l) {
        return process("float", parseInt(d[0].evaluated), parseInt(d[0].original), true);
      } }, { "name": "int$ebnf$1", "symbols": [/[0-9]/] }, { "name": "int$ebnf$1", "symbols": ["int$ebnf$1", /[0-9]/], "postprocess": function arrpush(d) {
        return d[0].concat([d[1]]);
      } }, { "name": "int", "symbols": ["int$ebnf$1"], "postprocess": function postprocess(d, l) {
        return process("int", d[0].join(""), d[0].join(""), true);
      } }, { "name": "_$ebnf$1", "symbols": [] }, { "name": "_$ebnf$1", "symbols": ["_$ebnf$1", /[\s]/], "postprocess": function arrpush(d) {
        return d[0].concat([d[1]]);
      } }, { "name": "_", "symbols": ["_$ebnf$1"], "postprocess": function postprocess(d, l) {
        return process(null, null, null, true);
      } }],
    ParserStart: "main"
  };
  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = grammar;
  } else {
    window.grammar = grammar;
  }
})();
//# sourceMappingURL=math.js.map
//# sourceMappingURL=math.js.map