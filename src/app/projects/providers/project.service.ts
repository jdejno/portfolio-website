import {Injectable, Type} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {LifelineComponent} from "../lifeline/lifeline.component";
import {UsaMapComponent} from "../usa-map/usa-map.component";
import {BitwiseComponent} from "../bitwise/bitwise.component";
import {LogoAnimationComponent} from "../logo-animation/logo-animation.component";

export interface IProject {
  projectID: string,
  title: string,
  description: string,
  image?: string,
  creationDate?: Date,
  lastUpdated?: Date
}

export class ProjectItem implements IProject {

  projectID: string;
  title: string;
  description: string;

  constructor(public component: Type<any>, public data: IProject) {}

}

/**
 * Service to return either all metadata for projects or a single ProjectComponent to be rendered.
 */
@Injectable()
export class ProjectService {

  private _cached: IProject[];

  constructor(private _http: Http) { }

  //#region public members
  /**
   * Return metadata of all projects
   * @returns {Promise<IProject[]>}
   */
  public getAllProjects(): Promise<IProject[]> {
    return this.getProjects();
  }

  public getProjects(numOfProjects?: number): Promise<IProject[]> {
    return new Promise((res, rej) => {
      this._http.get("/api/projects").map(resp => resp.json()).subscribe(data => {
        if(data === null || data === undefined) {
          rej("No projects were received");
          return;
        }
        this._cached = data.map(d => {
          return <IProject>{
            projectID: d.projectID,
            title: d.title,
            description: d.description,
            image: d.image
          }
        }).filter((d,i) => i < numOfProjects || data.length);  //filter to number of projects requested
        res(this._cached);
      })
    });
  }

  /**
   * Return a single project component to be rendered.
   * @param projectID - ID of post to be returned.
   * @returns {Promise<IProject>}
   */
  public getOneProject(projectID: string): Promise<ProjectItem> {
    return new Promise((res, rej) => {

      let projectData;
      //if we have cached data, use that insead of another http request
      if(this._cached !== undefined && this._cached !== null) {
        projectData = this._cached.find(o => o.projectID === projectID);
        res(new ProjectItem(this._getProjectComponent(projectID), projectData));
        return;
      }


      this._http.get(`/api/projects`).map(resp => resp.json()).subscribe(data => {
        if (data === null || data === undefined) {
          rej("No projects were received");
          return;
        }
        projectData = data.find(d => d.projectID ===  projectID);
        res(new ProjectItem(this._getProjectComponent(projectID), projectData))

      })
    })
  }



  //#endregion


  //#region private members

  private _getProjectComponent(projectID: string): Type<any> {
    return components.find(d => d.projectID === projectID).component;
  }

  //#endregion


}

//#region private data

//map of components based on projectID string
interface IComponent {projectID: string, component: Type<any>}
const components: IComponent[] = [
  { projectID: "Life-Line", component: LifelineComponent },
  { projectID: "USA-Map", component: UsaMapComponent },
  { projectID: "Expression-Evaluator", component: BitwiseComponent },
  { projectID: "Logo-Animation", component: LogoAnimationComponent}

];


//#endregion
