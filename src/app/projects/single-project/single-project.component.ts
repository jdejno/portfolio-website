import {Component, OnInit, ViewChild, ComponentFactoryResolver} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProjectService, ProjectItem} from "../providers/project.service";
import {ProjectHostDirective} from "../directives/project-host.directive";
import {trigger, state, transition, style, animate} from "@angular/animations";
import {NavbarSettingsService} from "../../navbar/navbar-settings.service";

declare let params: {
  projectID: string
};

/**
 * Helper Component to render any project into view.
 */
@Component({
  selector: 'app-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.scss'],
  animations: [
  trigger('fadeIn', [
    state('visible', style({opacity: '*'})),
    state('invisible', style({opacity: 0})),
    transition('invisible => visible', [
      style({opacity: 0}),
      animate("1000ms", style({opacity: '*'}))
    ])
  ])]
})
export class SingleProjectComponent implements OnInit {

  public projectID: string;
  public project: ProjectItem; //project component and metadata
  @ViewChild(ProjectHostDirective) projectHost: ProjectHostDirective; //anchor for rendering the project component
  public fadeInProject: string = 'invisible';

  constructor(private _route: ActivatedRoute,
              private _projectService: ProjectService,
              private _componentFactoryResolver: ComponentFactoryResolver,
              private _navbarSettings: NavbarSettingsService)
  {
    this._navbarSettings.fontColor = '#df691a';
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.projectID = params.projectID;
      this._projectService.getOneProject(this.projectID).then(res => {
        this.project = res;
        this.loadProject();
      })
    })
  }

  /**
   * Render project component using ComponentFactory
   */
  loadProject(): void {

    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(this.project.component);
    this.projectHost.viewContainerRef.clear();
    this.projectHost.viewContainerRef.createComponent(componentFactory);
    this.fadeInProject = 'visible';

  }

}
