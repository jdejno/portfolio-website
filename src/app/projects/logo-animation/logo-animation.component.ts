import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {LogoAnimationService, ELogoAnimations, Logo} from "./logo-animation.service";
import {GitCodeService, ECodeRepo} from "../../providers/git-code.service";


@Component({
  selector: 'app-logo-animation',
  templateUrl: './logo-animation.component.html',
  styleUrls: ['./logo-animation.component.scss'],
  providers: [
    LogoAnimationService
  ]
})
export class LogoAnimationComponent implements OnInit {

  public logo: Logo;
  @ViewChild('anchorDiv') anchor: ElementRef;
  @ViewChild('code') code: ElementRef;

  constructor(private _logoService: LogoAnimationService,
              private _gitCodeService: GitCodeService)
  {

  }

  ngOnInit() {

    this._logoService.appendLogo(this.anchor.nativeElement, 500).then((res) => {
      this.logo = res;
      this._logoService.animateLogo(this.logo, ELogoAnimations.BASIC, true);
    });

    let path = 'src/app/projects/logo-animation/logo-animation.service.ts';
    this._gitCodeService.appendPrettyPrintCode(this.code.nativeElement, ECodeRepo.BITBUCKET, 'portfolio-website', path);

  }

}
