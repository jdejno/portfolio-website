import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import {BaseType, ValueFn, Selection, selection} from "d3-selection";
import {CONSTANTS} from '../../../constants.service'
import {Transition} from "d3-transition";

//#region Interfaces Types  Enumerations

/**
 * Declare ambient interfaces for D3 definitions
 */
declare interface OldDatum {}
declare interface Datum {}

/**
 * Enumerated Logo Animation Options
 * BASIC - Default Animation. FLy in Arrows with letter draw.
 */
export enum ELogoAnimations {
  BASIC,
  LINE_EXPAND
}

/**
 * Logo Interface - All LogoSelections are public properties of ILogo
 */
interface ILogo {
  svg: LogoSelection;
  jPath: LogoSelection;
  dPath: LogoSelection;
  leftArrow: LogoSelection;
  firstRightArrow: LogoSelection;
  secondRightArrow: LogoSelection;
}

/**
 * Configuration options for Transitions
 */
export type TransitionConfig  = {
  duration?: number,
    color?: string,
    delay?: number
}

/**
 * Single Selection Element of Logo
 */
interface ILogoSelection {
  transition(): this;
  delay(delay: number): this;
  duration(duration: number): this;
  select(selector: string): LogoSelection;
  style(name: string, val: string | number): this;
  style(name: string, fun: ValueFn<BaseType, Datum, string | number | boolean | null>): this;
}

//#endregion


/**
 * Wrapper class for individual selections of Logo. Allows calling of chained transitions such as
 * logoSelection.transition().transition(), which will be executed in asynchronous order.
 */
export class LogoSelection implements ILogoSelection{

  //#region private members

  /*
  D3 Transition internally cached
   */
  private _transition: Transition<BaseType, Datum, BaseType, any> | undefined;
  /*
  SVGElement data (Path => d or Polygon => points)
   */
  private _svgData: string | void;

  //#endregion

  constructor(private _select: Selection<BaseType, Datum,BaseType, any>, private _constant?: CONSTANTS)
  {
    this._select = _select;
    this._constant = _constant;
    this._svgData = this._getSVGPointsOrD();

  }

  //#region public d3 overloaded methods

  /**
   * Creates and runs D3 Transition. Cached in _transition for reference.
   * @returns {LogoSelection}
   */
  transition(): this
  {
    this._transition = this._select.transition('trans').on('start', () => {
      this._transition = undefined;
    });
    return this;
  }

  /**
   * Adds time delay to D3 Transition stored in _transition.
   * @param delay - delay in ms
   * @returns {LogoSelection}
   */
  delay(delay: number): this
  {
    if(this._transition){
      this._transition.delay(delay)
    }
    return this;

  }

  /**
   * Specifies duration of D3 Transition stored in _transition.
   * @param duration - duration of transition in ms
   * @returns {LogoSelection}
   */
  duration(duration: number): this
  {
    if(this._transition){
      this._transition.duration(duration)
    }
    return this;
  }

  /**
   * Selects a child D3 Selection of this._select (cached reference of self D3 Selection)
   * @param selector {string} - CSS selector
   * @returns {Selection<BaseType, Datum, BaseType, any>}
   */
  select(selector: string): LogoSelection
  {
    return new LogoSelection(this._select.select(selector))
  }

  /**
   * Overloaded method for adding style values to D3 Transition stored in _transition.
   * @param name - name of style to transition.
   * @param val - new value to transition to.
   * returns {LogoSelection}
   */
  style(name: string, val: string | number): this;
  style(name: string, fun: ValueFn<BaseType, Datum, string | number | boolean | null>): this;
  style(name: string, val: string | number | ValueFn<BaseType, Datum, string | number | boolean | null>): this {

    if(this._transition){

      if(typeof val === 'string' || typeof val === 'number'){
        this._transition.style(name, <string>val);
        return this;
      }
      if(typeof val === 'function'){
        this._transition.style(name, <ValueFn<BaseType, Datum, string | number | boolean | null>>val);
        return this;
      }
    }

    return this;

  }

  attr(name: string, val: string | number): this
  {

    if(this._transition){

      this._transition.attr(name, val);

    }

    return this;
  }

  /**
   * Get self D3 Selection to handle
   * @returns {Selection<BaseType, Datum, BaseType, any>}
   */
  get selection(): Selection<BaseType, Datum,BaseType, any>
  {
    return this._select;
  }

  /**
   * Get current D3 Transition stored in _transition or create a new one and return it (via recursion)
   * @returns {Transition<BaseType, Datum, BaseType, any>|undefined|Transition<BaseType, Datum, BaseType, any>}
   */
  get currTransition(): Transition<BaseType, Datum, BaseType, any>
  {
    return this._transition || this.transition().currTransition;
  }

  /**
   * Get pathLength of SVGPathElement
   * @returns {number} - Length of Path Element
   */
  get pathLength(): number
  {
    return this._select.node() instanceof SVGPathElement ? (<SVGPathElement>this._select.node()).getTotalLength() : 0;
  }

  /**
   * Get d or points attribute of the LogoSelection
   * @returns {string|void}
   */
  get svgData(): string | void
  {
    return this._svgData;
  }
  //#endregion

  //#region public animations (run on this)

  /**
   * Transition this._select element's stroke to fully drawn. (If an this._select instanceof SVGPathElement)
   * @param config {TransitionConfig} - Transition Config options.
   * @returns {LogoSelection}
   */
  runAnimationStrokeOffset(config: TransitionConfig): this
  {
    if(this._select.node() instanceof SVGPathElement) {

      return this.transition()
        .delay(config.delay)
        .duration(config.duration)
        .style('stroke', config.color)
        .style('stroke-dasharray', this.pathLength)
        .style('stroke-dashoffset', 0);
    }

    return this;

  }

  /**
   * Transition this._select to origin spot of translate(0,0)
   * @param config {TransitionConfig} - Transition Config options.
   * @returns {LogoSelection}
   */
  runAnimationArrowTranslate(config: TransitionConfig): this
  {

    return this.transition()
      .delay(config.delay)
      .duration(config.duration)
      .style('stroke', config.color)
      .style('transform', `translate(0, 0)`);
  }

  //#endregion

  //#region private helpers

  private _getSVGPointsOrD(): string | void
  {
    if(this._select.node() instanceof SVGPathElement)
    {
      return this._select.attr('d');
    } else if (this._select.node() instanceof SVGGElement)
    {
      return this._select.select('polygon').attr('points');
    }

  }

  //#endregion
}


/**
 * SVG of Logo with parsed out polygons, and paths of logo SVG
 */
export class Logo implements ILogo
{

  //#region public members

  public svg: LogoSelection;
  public jPath: LogoSelection;
  public dPath: LogoSelection;
  public leftArrow: LogoSelection;
  public firstRightArrow: LogoSelection;
  public secondRightArrow: LogoSelection;
  public clipPathRect: Selection<BaseType, OldDatum, HTMLElement, any>;

  public clipPathX: number;
  public clipPathY: number;
  public clipPathHeight: number;
  public clipPathWidth: number;
  public size: number;

  //#endregion

  //region private members


  //#endregion

  constructor(svg: Selection<BaseType, OldDatum, HTMLElement, any>, private _constants: CONSTANTS)
  {

    this._constants = _constants;
    this.svg = new LogoSelection(svg);
    this.size = +svg.attr('width');
    this._parseLogoSVG();
    this._setResizeHandler();

  }

  //#region public methods

  //#region logo animations

  /**
   * Transition to fully drawn path using stroke-dasharray and stroke-dashoffset for LogoSelection
   * @param logoSelection - LogoSelection to fully draw stroke path
   * @param config {TransitionConfig} - Transition Config options
   * @returns {Promise<Logo>}
   */
  public runStrokeOffsetTransition(logoSelection: LogoSelection, config?: TransitionConfig): Promise<this>
  {

    config = this.getTransitionConfigOrDefault(config);

    return new Promise((res, rej) => {
      logoSelection.runAnimationStrokeOffset(config).currTransition.on('end', () => {
        res(this);
      });
    })

  }

  /**
   * Transition using translate(x, y) to origin (aka. translate(0, 0))
   * @param logoSelection - LogoSelection of Logo to transition
   * @param config {TransitionConfig} - Transition Config options
   * @returns {Promise<Logo>}
   */
  public runTranslateAnimation(logoSelection: LogoSelection, config? : TransitionConfig): Promise<this>
  {

    config = this.getTransitionConfigOrDefault(config);

    return new Promise((res, rej) => {
      logoSelection.runAnimationArrowTranslate(config).currTransition.on('end', () => {
        res(this);
      });
    });

  }

  /**
   * Transition all LogoSelections to specified style
   * @param name - name of style
   * @param val - value of style to set to.
   * @param config {TransitionConfig} - Transition Config options
   * @returns {Logo}
   */
  public applyStyleTransitionAll(name: string, val: number | string, config?: TransitionConfig): this
  {

    config = this.getTransitionConfigOrDefault(config);

    this.forEachSelection((logoSelection) => {
      logoSelection.currTransition
        .delay(config.delay)
        .duration(config.duration)
        .style(name, val);
    });

    return this;

  }

  /**
   * Apply function to all LogoSelections of this
   * @param fun {(logoSelection: LogoSelection) => void} - function to apply to all LogoSelections
   */
  public forEachSelection(fun: (logoSelection: LogoSelection) => void)
  {
    fun(this.jPath);
    fun(this.dPath);
    fun(this.leftArrow);
    fun(this.firstRightArrow);
    fun(this.secondRightArrow);
  }
  //#endregion

  //#endregion

  //#region private methods

  /**
   * set handler on window resize to translate Logo to center
   * @private
   */
  private _setResizeHandler()
  {

    window.addEventListener('resize', (ev) => {

      let width = window.innerWidth;
      this.svg.selection.style('transform', (d) => 'translate(' + (width - this.size) / 2 + 'px, 0)');

    });

  }

  /**
   * Using d3.selection.select(), get all g's and path's assigned to correct public members
   * @private
   */
  private _parseLogoSVG()
  {

    this.jPath = this.svg.select('#j');
    this.dPath = this.svg.select('#d');
    this.leftArrow = this.svg.select('#left-arrow');
    this.firstRightArrow = this.svg.select('#first-right-arrow');
    this.secondRightArrow = this.svg.select('#second-right-arrow');
    this.clipPathRect = this.svg.selection.select('clipPath').select('rect');
    this.clipPathX = +this.clipPathRect.attr('x');
    this.clipPathY = +this.clipPathRect.attr('y');
    this.clipPathHeight = +this.clipPathRect.attr('height');
    this.clipPathWidth = +this.clipPathRect.attr('width');

  }

  /**
   * Assign config value or defaults to config object.
   * @param config {TransitionConfig} - configuration object
   * @returns {TransitionConfig}
   * @private
   */
  public getTransitionConfigOrDefault(config: TransitionConfig): TransitionConfig
  {

    config = config || {};
    config.duration = config.duration || 1000;
    config.delay = config.delay || 0;
    config.color = config.color || this._constants.PRIMARY_COLOR || 'black';

    return config;
  }
  //#endregion

}

@Injectable()
export class LogoAnimationService
{


  constructor(private _constants: CONSTANTS) {
    this._constants = _constants;
  }

  //#region public methods

  /**
   * Append logo SVG to anchorDiv
   * @param anchorDiv: HTMLDivElement - div element to append logo svg to.
   * @param size: number - square dimensions of the logo.
   * @returns Promise<Logo> - Promise of Logo object.
   */
  public appendLogo(anchorDiv: HTMLDivElement,
                    size: number = 300): Promise<Logo>
  {

    return new Promise((res, rej) => {

      d3.xml(this._constants.LOGO_URL, xml => {
        let width = window.innerWidth;
        let logo = d3.select(xml.documentElement)
          .attr('width', size)
          .attr('height', size)
          .style('transform', (d, i) => 'translate(' + (width - size) / 2 + 'px, 0)');

        anchorDiv.appendChild(xml.documentElement);

        res(new Logo(logo, this._constants));

      });
    });

  }

  /**
   * Animate the logo based on designated animation in ELogoAnimation
   * @param logo: Logo - Logo object.
   * @param animation: ELogoAnimation - Animation type.
   * @param config {TransitionConfig} - Configuration Options for animation.
   * @param loop {boolean} - TRUE if looping transition infinite. Otherwise, run number of time specified.
   * @returns Promise<Logo> - Promise of Logo object once animation has completed.
   */
  public animateLogo(logo: Logo, animation: ELogoAnimations,loop: boolean | number = false, config?: TransitionConfig): Promise<Logo>
  {

    config = logo.getTransitionConfigOrDefault(config);

    return new Promise((res, rej) => {

      switch(animation)
      {
        case ELogoAnimations.BASIC:
          this._basicAnimation(logo, config, loop);
          break;
        case ELogoAnimations.LINE_EXPAND:
          this._lineExpandAnimation(logo, config, loop);
          break;
        default:
          break;
      }

    });

  }

  /**
   * Transition all LogoSelections of this Logo to opacity = 0;
   * @param logo - Logo to transition to opacity 0.
   * @param duration - Duration of transition in ms
   * @returns {Promise<Logo>}
   */
  public resetLogoTransition(logo: Logo, duration: number = 1000): Promise<Logo>
  {
    return new Promise(res => {
      logo.applyStyleTransitionAll('opacity', 0, {duration: duration})
        .secondRightArrow.currTransition.on('end', () => {
          res(logo)
      })
    });

  }

  /**
   * Reset LogoSelection styles (non-transition)
   * @param logo - Logo to reset LogoSelection styles for
   * @returns {LogoAnimationService}
   */
  public resetAllLogoSelectionStyles(logo: Logo): this
  {

    this.setAllLogoSelectionStyles(logo, [
      {name: 'opacity', val: 1},
      {name: 'stroke', val: 'transparent'},
      {name: 'fill', val: 'transparent'},
      {name: 'stroke-dasharray', val: false},
      {name: 'stroke-dashoffset', val: false},
      {name: 'fill-opacity', val: 1}

    ]);

    return this;
  }

  /**
   * Set styles on all LogoSelections of passed in Logo. (Not a transition. Synchronous style setting)
   * @param logo - Logo to set styles for
   * @param styles - Array of style objects
   * @returns {LogoAnimationService}
   */
  public setAllLogoSelectionStyles(logo: Logo, styles: {name: string, val: string | number | boolean}[]): this
  {
    for(let i = 0; i < styles.length; i++){

      logo.forEachSelection((logoSelection) => {

        logoSelection.selection.style(styles[i].name, styles[i].val);

      })

    }

    return this;
  }

  //#endregion

  //#region private methods

  /**
   * Basic Animation.
   * ORDER OF TRANSITIONS:
   *  1. Right Arrow enter
   *  2. Left Arrow enter
   *  3. J Path drawn
   *  4. D Path drawn
   *  5. Second Right Arrow appears + Logo Color change
   * @param logo - Logo to transition
   * @param config - TransitionConfig for first transition.
   * @param loop - number of times to loop or boolean to loop or not.
   * @private
   */
  private _basicAnimation(logo: Logo, config: TransitionConfig, loop: boolean | number = false)
  {
    this.resetAllLogoSelectionStyles(logo);

    logo.jPath.selection
      .style('stroke-width', 3)
      .style('fill-opacity', 0)
      .style('stroke-dasharray', logo.jPath.pathLength)
      .style('stroke-dashoffset', logo.jPath.pathLength);

    logo.dPath.selection
      .style('stroke-width', 3)
      .style('fill-opacity', 0)
      .style('stroke-dasharray', logo.dPath.pathLength)
      .style('stroke-dashoffset', logo.dPath.pathLength);

    logo.firstRightArrow.selection
      .style('stroke-width', 2)
      .style('transform', `translate(-200%, -200%)`);

    logo.secondRightArrow.selection
      .style('stroke-width', 2)
      .style('transform','translate(-45px, 0)');

    logo.leftArrow.selection
      .style('opacity', 1)
      .style('stroke-width', 2)
      .style('transform', 'translate(200%, 200%)');

    config.color = 'white';

    logo.runTranslateAnimation(logo.firstRightArrow, config).then((logo) => {
      logo.runTranslateAnimation(logo.leftArrow, {color: 'white'}).then((logo) => {
        logo.runStrokeOffsetTransition(logo.dPath, {
          duration: 750,
          delay: 500,
          color: 'white'
        });

        logo.runStrokeOffsetTransition(logo.jPath, {color: 'white'}).then((logo) => {
          logo.applyStyleTransitionAll('stroke', this._constants.PRIMARY_COLOR);

          logo.secondRightArrow.currTransition
              .style('stroke', this._constants.PRIMARY_COLOR)
              .style('transform', 'translate(0, 0)')
              .on('end', () => {
                this._runAnimationAgain(logo, config, loop, ELogoAnimations.BASIC);
            });

        });
      })
    });



  }

  //TODO: Finish line Expand Animation
  private _lineExpandAnimation(logo: Logo, config: TransitionConfig, loop: boolean | number = false)
  {

    this.resetAllLogoSelectionStyles(logo);
    this.setAllLogoSelectionStyles(logo, [
      {name: 'stroke', val: config.color},
      {name: 'stroke-width', val: 3}
    ]);

    let leftArrowLineData = LogoAnimationService._convertPolygonPointsToD(logo.leftArrow.svgData || '');
    let rightArrowLineData = LogoAnimationService._convertPolygonPointsToD(logo.firstRightArrow.svgData || '');
    let [x,y] = d3.polygonCentroid(leftArrowLineData);

    logo.svg.selection.append('circle').attr('cx', x).attr('cy',y).attr('r', 4);
    logo.leftArrow.selection.select('polygon')
      .transition()
      .delay(1000)
      .duration(1000)
      .attr('points', [0, y, x, y, 0, y, x, y].join(' '));

    [x,y] = d3.polygonCentroid(rightArrowLineData);
    logo.svg.selection.append('circle').attr('cx', x).attr('cy',y).attr('r', 4);
    logo.firstRightArrow.select('polygon')
      .transition()
      .delay(1000)
      .duration(1000)
      .attr('points', [x, y, x, y, x, y, x, y, x, y, x, y].join(' '));

    logo.secondRightArrow.selection.select('polygon')
      .transition()
      .delay(500)
      .duration(1000)
      .attr('points', [x, logo.size / 2, x, logo.size / 2].join(' '));

  }

  /**
   * Run animation again based on 'loop' parameter.
   * NOTE: If loop is a number, it will decrement by 1 each time as it recursively calls animateLogo
   * @param logo {Logo} - Logo to animate
   * @param config {TransitionConfig} - Transition Config options
   * @param loop {number | boolean} - number of times to loop. Or, if boolean, don't loop or loop infinitely.
   * @param animation {ELogoAnimations} - Type of logo animation to run
   * @private
   */
  private _runAnimationAgain(logo: Logo, config: TransitionConfig, loop: number | boolean, animation: ELogoAnimations)
  {

    if((typeof loop === 'boolean' && loop) || (typeof loop === 'number' && loop > 1)) {

      setTimeout(() => {

        this.resetLogoTransition(logo).then(logo => {

          if (typeof loop === 'number') {

            this.animateLogo(logo, animation, --loop, config);

          } else {

            this.animateLogo(logo, animation, loop, config)

          }

        });

      }, 1000);
    }
  }

  private static _convertPolygonPointsToD(points: string): [number, number][]
  {
    let pointsArray = points.split(' ');
    let res = [];
    for(let i = 0; i < pointsArray.length - 1; i++)
    {
      res.push([+pointsArray[i], +pointsArray[++i]]);
    }

    return res;

  }

  //#endregion

}

