import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LogoAnimationComponent} from "./logo-animation.component";
import {LogoAnimationService} from "./logo-animation.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LogoAnimationComponent
  ],
  providers: [
    LogoAnimationService
  ]
})
export class LogoAnimationModule { }
export { ELogoAnimations, LogoSelection, Logo, LogoAnimationService } from "./logo-animation.service";
