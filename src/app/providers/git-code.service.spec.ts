import { TestBed, inject } from '@angular/core/testing';

import { GitCodeService } from './git-code.service';

describe('GitCodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GitCodeService]
    });
  });

  it('should be created', inject([GitCodeService], (service: GitCodeService) => {
    expect(service).toBeTruthy();
  }));
});
