import { Injectable } from '@angular/core';
import {Http, URLSearchParams, Response} from "@angular/http";
import {Observable} from 'rxjs' ;

declare let hljs: any;

//#region Enums / Types / Interfaces

/**
 * Enum for code repo platforms
 */
export enum ECodeRepo {
  BITBUCKET,
  GITHUB
}

/**
 * Enum for Lexer to use for code highlighting
 */
export enum ECodeLexerOptions {
  TYPESCRIPT,
  JAVASCRIPT
}

//#endregion

@Injectable()
export class GitCodeService {

  //#region private members

  //Map of ECodeLexerOptions to string for server to use.
  private _lexerMap: Map<ECodeLexerOptions, string>;

  //#endregion

  //#region Ctor

  constructor(private _http: Http)
  {

    this._lexerMap = new Map()
      .set(ECodeLexerOptions.TYPESCRIPT, 'typescript');
  }

  //#endregion


  //#region public methods

  /**
   * Append formatted code (html) from remote repos (Github or Bitbukect) to provided HTMLElement (wrapped in <pre></pre>)
   * @param el {HTMLElement} - HTML element to append formatted code to (html)
   * @param git {ECodeRepo} - Remote Repo platform
   * @param repo {string} - name of remote repo
   * @param path {string} - absolute path in remote repo
   * @returns {Promise<boolean>}
   */
  appendPrettyPrintCode(el: HTMLDivElement, git: ECodeRepo, repo: string, path: string): Promise<boolean>
  {
    return new Promise((res, rej) => {
      this.getPrettyPrintCode(git, repo, path).then(html => {
        let pre = document.createElement('pre');
        pre.innerHTML = html;
        el.appendChild(pre);
      });
    });

  }

  /**
   * Returns raw text of code from remote repo.
   * @param git {ECodeRepo} - Remote repo platform
   * @param repo {string} - name of remote repo
   * @param path {string} - absolute path to file in remote repo
   * @param user {string} - owner of repo
   * @returns {Promise<string>} - raw text of code
   */
  getRawCodeFile(git: ECodeRepo, repo: string, path: string, user: string = 'jdejno'): Promise<string>
  {
    path = path.replace(/(^\/)/, ''); //strip leading slash
    let url = encodeURI(`/api/code/raw/${git}/${repo}`);
    let queryParams = new URLSearchParams();
    queryParams.set('user', user);
    queryParams.set('path', encodeURI(path));

    return new Promise((res, rej) => {

      this._http.get(url, {params: queryParams}).map(data => data.text()).subscribe((data) => {
          if(data){
            res(data);
          } else {
            rej('No file found for ' + url);
          }
        });
    });

  }

  /**
   * Get formatted code in html with styling from remote repos
   * @param git {ECodeRepo} - Code repo platform
   * @param repo {string} - remote repo name
   * @param path {string} - absolute path to file in remote repo
   * @param lexer {ECodeLexerOptions} - code lexer to use for highlight during formatting
   * @param user {string} - Owner of repo
   * @returns {Observable<Response>}
   */
  getPrettyPrintCode(git: ECodeRepo, repo: string, path: string, lexer: ECodeLexerOptions = ECodeLexerOptions.TYPESCRIPT, user: string = 'jdejno'): Promise<string>
  {

    return new Promise((res, rej) => {

      this.getRawCodeFile(git, repo, path, user).then(code => {

          let prettyCode = hljs.highlight(this._lexerMap.get(lexer), code, true);
          res(prettyCode.value);

        }).catch(reject => {

          rej(reject);

      })

    })



  }

  //#endregion

}
