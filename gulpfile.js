/**
 * Created by jeremydejno on 5/7/17.
 */
const gulp = require('gulp');
const pug = require('gulp-pug');
const watch = require('gulp-watch');
const gulp_watch_pug = require('gulp-watch-pug');
const nearley = require('gulp-nearley');
const babel = require("gulp-babel");

// gulp.task('babel', () =>
//   gulp.src('src/**/*.js')
//     .pipe(babel({
//       presets: ['env']
//     }))
//     .pipe(gulp.dest('.'))
// );

gulp.task('build-pug', (done) => {
  "use strict";
  return gulp.src('./src/app/**/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./src/app'))
});

gulp.task('watch-pug', (done) =>
  gulp.src('./src/app/**/*.pug')
    .pipe(watch('./src/app/**/*.pug'))
    .pipe(gulp_watch_pug('./src/app/**/*.pug', { delay: 3000 }))
    .pipe(pug())
    .pipe(gulp.dest('./src/app'))
);


gulp.task('nearley', () =>
  gulp.src('./src/app/**/*.ne')
    .pipe(nearley())
    .pipe(gulp.dest('./src/app'))
);

gulp.task('default', ['build-pug']);
